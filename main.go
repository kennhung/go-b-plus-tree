package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/kennhung/ccu-database-course/bTree"
)

var bt *bTree.BTree

func Initialize(order int) {
	bt = bTree.NewBTree(order * 2)
}

func Attach(input string) {
	if bt == nil {
		panic("B+ tree not initalized")
	}

	nodes := strings.Split(input, ";")
	isLeaf := true

	leafs := []bTree.AttachNums{}
	internals := []int{}

	for _, v := range nodes {
		nums := []int{}

		numStrs := strings.Split(v, " ")
		for _, v2 := range numStrs {
			if len(v2) > 0 {
				n, _ := strconv.Atoi(strings.ReplaceAll(v2, "\n", ""))
				nums = append(nums, n)
			}
		}

		if isLeaf {
			leafs = append(leafs, nums)
		} else {
			internals = append(internals, nums[0])
		}

		isLeaf = !isLeaf
	}

	bt.Attach(leafs, internals)
}

func Bulkload(numStrs []string) error {
	if bt == nil {
		panic("B+ tree not initalized")
	}

	nums := []int{}
	for _, v := range numStrs {
		n, err := strconv.Atoi(strings.ReplaceAll(v, "\n", ""))
		if err != nil {
			return err
		}

		nums = append(nums, n)
	}

	for _, v := range nums {
		bt.Insert(v)
	}

	return nil
}

func Lookup(k int) bool {
	if bt == nil {
		panic("B+ tree not initalized")
	}

	return bt.Search(k)
}

func Insert(k int) {
	if bt == nil {
		panic("B+ tree not initalized")
	}

	bt.Insert(k)
}

func Delete(k int) {
	if bt == nil {
		panic("B+ tree not initalized")
	}

	bt.Delete(k)
}

func Display() {
	if bt == nil {
		panic("B+ tree not initalized")
	}

	bt.InorderPrint()
	// bt.PrintTreeGraphviz(os.Stdout)
}

func printOperations() {
	for i, v := range []string{
		"Initialize",
		"Attach",
		"Bulkload",
		"Lookup",
		"Insert",
		"Delete",
		"Display",
		"Quit",
	} {
		fmt.Printf("%d) %s\n", i+1, v)
	}
}

func runControl() {
	for {
		fmt.Println()
		printOperations()
		fmt.Printf("Select an operation > ")

		var selected int
		fmt.Scanf("%d", &selected)
		fmt.Println()

		switch selected {
		case 1:
			fmt.Println("Initalizing b+ tree")
			fmt.Print("order = ")
			var order int
			fmt.Scanf("%d", &order)
			fmt.Println()

			if order < 1 {
				fmt.Println("Wrong order number")
				break
			}

			Initialize(order)
			fmt.Println("Initialization complete.")
		case 2:
			fmt.Print("Attaching... order = ")
			var order int
			fmt.Scanf("%d", &order)

			fmt.Print("Nodes in inorder-like traversal > ")

			reader := bufio.NewReader(os.Stdin)
			in, _ := reader.ReadString('\n')

			Initialize(order)
			Attach(in)
		case 3, 4, 5, 6, 7:
			if bt == nil {
				fmt.Println("You need to initalize the tree first.")
				break
			}
			switch selected {
			case 3:
				fmt.Println("Bulkloading...")
				fmt.Print("Enter keys > ")

				reader := bufio.NewReader(os.Stdin)
				in, _ := reader.ReadString('\n')

				numStrs := strings.Split(in, " ")
				err := Bulkload(numStrs)
				if err != nil {
					fmt.Println(err)
					break
				}

				fmt.Println("Bulkload complete.")
			case 4:
				fmt.Print("Lookup key = ")
				var number int
				fmt.Scanf("%d", &number)

				fmt.Printf("Key %d ", number)
				if Lookup(number) {
					fmt.Println("found in the tree.")
				} else {
					fmt.Println("not found in the tree.")
				}
			case 5:
				fmt.Printf("Inserting key = ")
				var number int
				fmt.Scanf("%d", &number)

				Insert(number)
				fmt.Println("Insert complete.")
			case 6:
				fmt.Printf("Deleting key = ")
				var number int
				fmt.Scanf("%d", &number)

				Delete(number)
				fmt.Println("Delete complete.")
			case 7:
				fmt.Println("Displaying tree")

				Display()
			}
		case 8:
			fmt.Println("Bye~")
			return
		default:
			fmt.Println("Wrong operation number.")
		}
	}
}

func webServer() {
	// http.Handle("/lib/", http.StripPrefix("/lib/", http.FileServer(http.Dir("./lib"))))

	http.HandleFunc("/graphviz", func(rw http.ResponseWriter, r *http.Request) {
		if bt != nil {
			bt.PrintTreeGraphviz(rw)
		} else {
			http.Error(rw, "", http.StatusNoContent)
		}
	})
	http.HandleFunc("/init", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}

		body := struct {
			Order int `json:"order"`
		}{}

		bodyBytes, _ := ioutil.ReadAll(r.Body)

		err := json.Unmarshal(bodyBytes, &body)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		if body.Order <= 0 {
			http.Error(rw, "Error: order must bigger than zero.", http.StatusBadRequest)
			return
		}

		Initialize(body.Order)
	})
	http.HandleFunc("/attach", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}

		body := struct {
			Order int    `json:"order"`
			Str   string `json:"attachStr"`
		}{}

		bodyBytes, _ := ioutil.ReadAll(r.Body)

		err := json.Unmarshal(bodyBytes, &body)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		if body.Order <= 0 {
			http.Error(rw, "Error: order must bigger than zero.", http.StatusBadRequest)
			return
		}

		Initialize(body.Order)
		Attach(body.Str)
	})
	http.HandleFunc("/bulkload", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}

		body := struct {
			NumStrs string `json:"numStrs"`
		}{}

		bodyBytes, _ := ioutil.ReadAll(r.Body)

		err := json.Unmarshal(bodyBytes, &body)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		Bulkload(strings.Split(body.NumStrs, " "))
	})
	http.HandleFunc("/lookup", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}

		body := struct {
			Number int `json:"number"`
		}{}

		bodyBytes, _ := ioutil.ReadAll(r.Body)

		err := json.Unmarshal(bodyBytes, &body)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		fmt.Fprint(rw, Lookup(body.Number))
	})
	http.HandleFunc("/insert", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}

		body := struct {
			Number int `json:"number"`
		}{}

		bodyBytes, _ := ioutil.ReadAll(r.Body)

		err := json.Unmarshal(bodyBytes, &body)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		Insert(body.Number)
	})
	http.HandleFunc("/delete", func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			return
		}

		body := struct {
			Number int `json:"number"`
		}{}

		bodyBytes, _ := ioutil.ReadAll(r.Body)

		err := json.Unmarshal(bodyBytes, &body)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusInternalServerError)
			return
		}

		Delete(body.Number)
	})
	http.HandleFunc("/quit", func(rw http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(rw, "Bye~")
		os.Exit(0)
	})

	http.HandleFunc("/index.html", func(w http.ResponseWriter, r *http.Request) {
		f, err := os.Open("index.html")
		if err != nil {
			// handle error
			return
		}
		http.ServeContent(w, r, "index.html", time.Now(), f)
	})

	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			http.Redirect(rw, r, "/index.html", http.StatusMovedPermanently)
		}
	})

	var portNum int
	fmt.Print("Enter port number for using GUI (default is 9090) : ")
	fmt.Scanf("%d", &portNum)

	if portNum == 0 {
		portNum = 9090
	}

	fmt.Printf("Open browser and enter http://127.0.0.1:%d\n (ip address may be different if not running on localhost)\n\n", portNum)
	go http.ListenAndServe(fmt.Sprintf(":%d", portNum), nil)
}

func runApp() {
	webServer()

	fmt.Println("Type \"hello\" if you want to use termial to control the tree.")
	for {
		var str string
		fmt.Scanf("%s", &str)

		if str == "hello" {
			break
		}
	}

	runControl()
}

func printTreeTo(path string) {
	fo, err := os.Create(path)
	if err != nil {
		panic(err)
	}

	fmt.Fprint(fo, "digraph  {\nnode [shape = record,height=.1];\n")
	bt.PrintTreeGraphviz(fo)
	fmt.Fprint(fo, "}")
	fo.Close()
}

func runHW6E() {
	Initialize(1)

	for i, v := range []int{4, 12, 6, 1, 3, 15, 5, 9, 7, 14, 16, 10, 11} {
		Insert(v)

		printTreeTo(fmt.Sprintf("./tmp/step_%d.dot", i))
	}
}

func runHW6F() {
	Initialize(2)
	for i := 1; i <= 7; i++ {
		Insert(2*i - 1)
		Insert(2 * i)

		printTreeTo(fmt.Sprintf("./tmp/step_%d.dot", i))
	}

	Insert(15)
	printTreeTo(fmt.Sprintf("./tmp/step_%d.dot", 8))
}

func runHW6G() {
	Initialize(1)
	Attach("1;3;3 9;10;10;17;17;38;38 52")

	for i, v := range []int{1, 17, 9, 10, 3} {
		Delete(v)

		printTreeTo(fmt.Sprintf("./tmp/step_%d.dot", i))
	}
}

func runHW6H() {
	Initialize(1)
	Attach("1 3;5;5 7;9;9 11; 13; 13 15;17;17")

	for i, v := range []int{13, 15, 17} {
		Delete(v)

		printTreeTo(fmt.Sprintf("./tmp/step_%d.dot", i))
	}
}

func main() {
	// runApp()

	// runHW6E()

	// runHW6F()

	// runHW6G()

	runHW6H()

	// Bulkload(strings.Split("46 10 70 49 23 40 59 29 34 54 75 30", " "))

	// bt.PrintTree()
	// for _, v := range []int{
	// 	29, 30, 34, 46, 49, 40, 54, 70,
	// } {
	// 	Delete(v)
	// }

	// bt.PrintTree()
	// for _, v := range []int{
	// 	70, 470,
	// } {
	// 	fmt.Println()
	// 	fmt.Printf("Deleting %d\n", v)
	// 	Delete(v)
	// 	bt.PrintTree()
	// 	// bt.PrintTreeGraphviz(os.Stdout)
	// }
}
