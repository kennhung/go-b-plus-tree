package bTree

import (
	"fmt"
	"io"
	"math"
	"sort"
)

func checkIsTooManyKey(len int, order int) bool {
	return len > order
}

func checkIsTooFewKey(len int, order int) bool {
	return len <= int(math.Ceil(float64(order)/2.0)-1)
}

func insertLeafData(arr *[]LeafData, pos int, v LeafData) {
	if pos >= len(*arr) {
		*arr = append(*arr, v)
	} else {
		*arr = append(*arr, LeafData{}) // add one capacity for new element
		copy((*arr)[pos+1:], (*arr)[pos:])
		(*arr)[pos] = v
	}
}

func removeLeafData(slice []LeafData, s int) []LeafData {
	return append(slice[:s], slice[s+1:]...)
}

func insertInt(arr *[]int, pos int, v int) {
	if pos >= len(*arr) {
		*arr = append(*arr, v)
	} else {
		*arr = append(*arr, 0) // add one capacity for new element
		copy((*arr)[pos+1:], (*arr)[pos:])
		(*arr)[pos] = v
	}
}

func removeInt(slice []int, s int) []int {
	if len(slice) > s+1 {
		return append(slice[:s], slice[s+1:]...)
	}
	return slice[:s]
}

func insertNode(arr *[]Node, pos int, v Node) {
	if pos >= len(*arr) {
		*arr = append(*arr, v)
	} else {
		*arr = append(*arr, nil) // add one capacity for new element
		copy((*arr)[pos+1:], (*arr)[pos:])
		(*arr)[pos] = v
	}
}

func removeNode(slice []Node, s int) []Node {
	if len(slice) > s+1 {
		return append(slice[:s], slice[s+1:]...)
	}
	return slice[:s]
}

type BTree struct {
	root  Node
	order int
}

func NewBTree(order int) *BTree {
	t := new(BTree)
	t.order = order
	t.root = &LeafNode{
		order: order,
	}

	return t
}

func (t *BTree) Search(k int) bool {
	leaf := t.root.search(k)
	for _, v := range leaf.data {
		if v.key == k {
			return true
		}
	}
	return false
}

type AttachNums []int

func (att AttachNums) toLeaf() *LeafNode {
	leaf := &LeafNode{}

	for _, v := range att {
		leaf.data = append(leaf.data, LeafData{
			key: v,
		})
	}

	return leaf
}

func (t *BTree) Attach(leafs []AttachNums, internals []int) {
	if len(internals) == 0 {
		// only one leaf node as root
		leaf := leafs[0].toLeaf()
		leaf.order = t.order
		t.root = leaf
	} else {
		nd := &InternalNode{}
		nd.order = t.order
		t.root = nd

		prevLeaf := leafs[0].toLeaf()
		prevLeaf.order = t.order
		prevLeaf.parent = nd

		nd.children = append(nd.children, prevLeaf)

		for i, v := range internals {
			leaf := leafs[i+1].toLeaf()
			leaf.order = t.order
			leaf.parent = nd
			prevLeaf.nextNode = leaf

			nrt := nd.insert(v, leaf)
			if nrt != nil {
				t.root = nrt
			}
			prevLeaf = leaf

			nd = t.root.(*InternalNode).getMostDownRightNode()
		}
	}
}

func (t *BTree) Insert(k int) {
	leaf := t.root.search(k)

	newRoot := leaf.insert(k, nil)
	if newRoot != nil {
		t.root = newRoot
	}
}

func (t *BTree) Delete(k int) {
	leaf := t.root.search(k)

	newRoot := leaf.delete(k)
	if newRoot != nil {
		if rt, ok := newRoot.(*InternalNode); ok {
			rt.parent = nil
		} else if rt, ok := newRoot.(*LeafNode); ok {
			rt.parent = nil
		}

		t.root = newRoot
	}
}

func (t *BTree) GetFirst() *LeafNode {
	return t.root.getFirst()
}

func (t *BTree) PrintTree() {
	nlNode := &InternalNode{}

	var que []Node
	que = append(que, t.root, nlNode)

	for len(que) > 0 {
		node := que[0]
		que = que[1:]

		if node == nlNode {
			fmt.Println()
			continue
		}

		if node != nil {
			switch node2 := node.(type) {
			case *InternalNode:
				fmt.Print(node2.keys)
			case *LeafNode:
				fmt.Print(node2.data)
			default:
				panic("wrong node type")
			}
		} else {
			fmt.Println(nil)
		}

		if nd, ok := node.(*InternalNode); ok {
			que = append(que, nd.children...)
			if len(que) > 0 && que[0] == nlNode {
				que = append(que, nlNode)
			}
		}
	}
}

func (t *BTree) PrintTreeGraphviz(w io.Writer) {
	var que []Node
	que = append(que, t.root)

	for len(que) > 0 {
		node := que[0]
		que = que[1:]

		if node != nil {
			switch node2 := node.(type) {
			case *InternalNode:
				if len(node2.keys) == 0 {
					break
				}
				fmt.Fprintf(w, "node%p[label = \"<f0>", node2)

				for i, v := range node2.keys {
					fmt.Fprintf(w, " |%d|<f%d>", v, i+1)
				}

				fmt.Fprint(w, "\"];\n")

				if node2.parent != nil {
					i := 0
					for ; i < len(node2.parent.children); i++ {
						if node2.parent.children[i] == node2 {
							fmt.Fprintf(w, "\"node%p\":f%d -> node%p\n", node2.parent, i, node2)
							break
						}
					}
				}
			case *LeafNode:
				if len(node2.data) == 0 {
					break
				}
				fmt.Fprintf(w, "node%p[label = \"", node2)

				for i, v := range node2.data {
					if i != 0 {
						fmt.Fprint(w, "|")
					}
					fmt.Fprintf(w, "%d", v.key)
				}

				fmt.Fprint(w, "\"];\n")

				if node2.parent != nil {
					i := 0
					for ; i < len(node2.parent.children); i++ {
						if node2.parent.children[i] == node2 {
							fmt.Fprintf(w, "\"node%p\":f%d -> node%p\n", node2.parent, i, node2)
							break
						}
					}
				}
			default:
				panic("wrong node type")
			}
		}

		if nd, ok := node.(*InternalNode); ok {
			que = append(que, nd.children...)
		}
	}
}

func (t *BTree) InorderPrint() {
	t.root.inOrderPrint()
	fmt.Println()
}

type Node interface {
	search(k int) *LeafNode
	insert(k int, node Node) *InternalNode
	isOverflow() bool
	isTooFew() bool
	getFirst() *LeafNode
	getLeft() Node
	getRight() Node
	inOrderPrint()
}

type InternalNode struct {
	keys     []int
	children []Node

	order  int
	parent *InternalNode
}

func (nd *InternalNode) search(k int) *LeafNode {
	// find upper bound of k in keys
	i := sort.Search(len(nd.keys), func(i int) bool {
		return nd.keys[i] > k
	})

	return nd.children[i].search(k)
}

func (nd *InternalNode) insert(k int, newNode Node) *InternalNode {
	i := sort.Search(len(nd.keys), func(i int) bool {
		return nd.keys[i] > k
	})
	insertInt(&nd.keys, i, k)
	insertNode(&nd.children, i+1, newNode)

	if nd.isOverflow() {
		breakPoint := nd.order / 2

		nd2 := new(InternalNode)
		nd2.order = nd.order
		nd2.parent = nd.parent

		breakPointKey := nd.keys[breakPoint]

		nd2.keys = append(nd2.keys, nd.keys[breakPoint+1:]...)
		nd.keys = nd.keys[:breakPoint]

		nd2.children = append(nd2.children, nd.children[breakPoint+1:]...)
		nd.children = nd.children[:breakPoint+1]

		for _, v := range nd2.children {
			if newNode, ok := v.(*InternalNode); ok {
				newNode.parent = nd2
			} else if leafNode, ok := v.(*LeafNode); ok {
				leafNode.parent = nd2
			}
		}

		if nd.parent == nil {
			newRoot := new(InternalNode)
			newRoot.order = nd.order
			newRoot.keys = append(newRoot.keys, breakPointKey)
			newRoot.children = append(newRoot.children, nd, nd2)

			nd.parent = newRoot
			nd2.parent = newRoot

			return newRoot
		}

		return nd.parent.insert(breakPointKey, nd2)
	}

	return nil
}

func (nd *InternalNode) update() Node {

	// fmt.Println(nd.keys, nd.children)
	// for _, v := range nd.children {
	// 	if v2, ok := v.(*InternalNode); ok {
	// 		fmt.Println(v2.keys)
	// 	} else if v2, ok := v.(*LeafNode); ok {
	// 		fmt.Println(v2.data)
	// 	}
	// }

	for len(nd.keys) > 0 {
		first := nd.children[0].getFirst()
		if first == nil || len(first.data) == 0 {
			nd.keys = removeInt(nd.keys, 0)
			nd.children = removeNode(nd.children, 0)
		} else {
			break
		}
	}

	// check childs and update keys
	for i := 0; i < len(nd.keys); {
		firstLeaf := nd.children[i+1].getFirst()

		if firstLeaf == nil || len(firstLeaf.data) == 0 {
			nd.keys = removeInt(nd.keys, i)
			nd.children = removeNode(nd.children, i+1)
		} else {
			nd.keys[i] = nd.children[i+1].getFirst().data[0].key
			i++
		}
	}

	if nd.parent == nil {
		// Is root
		if len(nd.keys) < 1 {
			return nd.children[0]
		}

		return nil
	} else if nd.isTooFew() {
		// Not root and too few key, need to borrow or merge

		var leftNode, rightNode *InternalNode
		if nil != nd.getLeft() {
			leftNode, _ = nd.getLeft().(*InternalNode)
		}
		if nil != nd.getRight() {
			rightNode, _ = nd.getRight().(*InternalNode)
		}

		ndIdx := nd.getChildIdx()

		if leftNode != nil && !checkIsTooFewKey(len(leftNode.keys)-1, nd.order) {
			// if left is available, borrow from left

			{
				i := len(leftNode.children) - 1

				if nd2, ok := leftNode.children[i].(*InternalNode); ok {
					nd2.parent = nd
				} else if nd2, ok := leftNode.children[i].(*LeafNode); ok {
					nd2.parent = nd
				}

				insertNode(&(nd.children), 0, leftNode.children[i])
				leftNode.children = removeNode(leftNode.children, i)
			}

			insertInt(&(nd.keys), 0, nd.parent.keys[ndIdx-1])

			{
				i := len(leftNode.keys) - 1
				nd.parent.keys[ndIdx-1] = leftNode.keys[i]
				leftNode.keys = removeInt(leftNode.keys, i)
			}
		} else if rightNode != nil && !checkIsTooFewKey(len(rightNode.keys)-1, nd.order) {
			// else if right is available, borrow from right

			{
				if nd2, ok := rightNode.children[0].(*InternalNode); ok {
					nd2.parent = nd
				} else if nd2, ok := rightNode.children[0].(*LeafNode); ok {
					nd2.parent = nd
				}

				insertNode(&(nd.children), len(nd.children), rightNode.children[0])
				rightNode.children = removeNode(rightNode.children, 0)
			}

			insertInt(&(nd.keys), len(nd.keys), nd.parent.keys[ndIdx])

			{
				nd.parent.keys[ndIdx] = rightNode.keys[0]
				rightNode.keys = removeInt(rightNode.keys, 0)
			}
		} else {
			// else merge with left or right

			if leftNode != nil {
				leftNode.keys = append(leftNode.keys, nd.parent.keys[ndIdx-1])
				leftNode.keys = append(leftNode.keys, nd.keys...)

				for i := range nd.children {
					if nd2, ok := nd.children[i].(*InternalNode); ok {
						nd2.parent = leftNode
					}

					if nd2, ok := nd.children[i].(*LeafNode); ok {
						nd2.parent = leftNode
					}
				}
				leftNode.children = append(leftNode.children, nd.children...)

				nd.keys = nd.keys[:0]
				nd.children = nd.children[:0]
			} else if rightNode != nil {
				rightNode.keys = append([]int{nd.parent.keys[ndIdx]}, rightNode.keys...)
				rightNode.keys = append(nd.keys, rightNode.keys...)

				for i := range nd.children {
					if nd2, ok := nd.children[i].(*InternalNode); ok {
						nd2.parent = rightNode
					}

					if nd2, ok := nd.children[i].(*LeafNode); ok {
						nd2.parent = rightNode
					}
				}
				rightNode.children = append(nd.children, rightNode.children...)

				nd.keys = nd.keys[:0]
				nd.children = nd.children[:0]
			} else {
				panic("error: no node to merge")
			}
		}
	}

	return nd.parent.update()
}

func (nd *InternalNode) isOverflow() bool {
	return checkIsTooManyKey(len(nd.keys), nd.order)
}

func (nd *InternalNode) isTooFew() bool {
	return checkIsTooFewKey(len(nd.keys), nd.order)
}

func (nd *InternalNode) getFirst() *LeafNode {
	if len(nd.children) > 0 && nd.children[0] != nil {
		return nd.children[0].getFirst()
	}
	return nil
}

func (nd *InternalNode) getChildIdx() int {
	if nd.parent == nil {
		return -1
	}

	i := 0
	for i < len(nd.parent.children) && nd.parent.children[i] != nd {
		i++
	}

	if nd.parent.children[i] != nd {
		panic("not found in children")
	}

	return i
}

func (nd *InternalNode) getOffset(offset int) Node {
	if nd.parent == nil {
		return nil
	}

	i := nd.getChildIdx()

	if i+offset >= 0 && i+offset < len(nd.parent.children) {
		if nd2, ok := nd.parent.children[i+offset].(*InternalNode); ok {
			return nd2
		}
		panic("not internal node")
	}
	return nil
}

func (nd *InternalNode) getLeft() Node {
	return nd.getOffset(-1)
}

func (nd *InternalNode) getRight() Node {
	return nd.getOffset(1)
}

func (nd *InternalNode) getMostDownRightNode() *InternalNode {
	lastChild := nd.children[len(nd.children)-1]

	if lc, ok := lastChild.(*InternalNode); lastChild != nil && ok {
		return lc.getMostDownRightNode()
	}

	return nd
}

func (nd *InternalNode) inOrderPrint() {
	nd.children[0].inOrderPrint()
	fmt.Print(" ; ")
	for i, v := range nd.keys {
		if i != 0 {
			fmt.Print(" ; ")
		}
		fmt.Printf("%d ; ", v)
		nd.children[i+1].inOrderPrint()
	}
}

type LeafData struct {
	key int
}

type LeafNode struct {
	data     []LeafData
	nextNode *LeafNode
	prevNode *LeafNode

	order  int
	parent *InternalNode
}

func (leaf *LeafNode) search(k int) *LeafNode {
	// when searching on leaf, just return leaf
	return leaf
}

func (leaf *LeafNode) insert(k int, _ Node) *InternalNode {
	i := sort.Search(len(leaf.data), func(i int) bool {
		return leaf.data[i].key > k
	})

	insertLeafData(&leaf.data, i, LeafData{
		key: k,
	})

	if leaf.isOverflow() {
		breakPoint := leaf.order / 2

		leaf2 := new(LeafNode)
		leaf2.order = leaf.order
		leaf2.parent = leaf.parent
		leaf2.data = append(leaf2.data, leaf.data[breakPoint:]...)
		leaf.data = leaf.data[:breakPoint]

		leaf2.nextNode = leaf.nextNode
		if leaf2.nextNode != nil {
			leaf2.nextNode.prevNode = leaf2
		}
		leaf.nextNode = leaf2
		leaf2.prevNode = leaf

		if leaf.parent == nil {
			newRoot := new(InternalNode)
			newRoot.order = leaf.order
			newRoot.keys = append(newRoot.keys, leaf2.data[0].key)
			newRoot.children = append(newRoot.children, leaf, leaf2)

			leaf.parent = newRoot
			leaf2.parent = newRoot

			return newRoot
		}
		return leaf.parent.insert(leaf2.data[0].key, leaf2)
	}

	return nil
}

func (leaf *LeafNode) isOverflow() bool {
	return checkIsTooManyKey(len(leaf.data), leaf.order)
}

func (leaf *LeafNode) isTooFew() bool {
	return checkIsTooFewKey(len(leaf.data), leaf.order)
}

func (leaf *LeafNode) delete(k int) Node {
	i := 0
	for i < len(leaf.data) {
		if leaf.data[i].key == k {
			break
		}
		i++
	}

	if i < len(leaf.data) {
		return leaf.deleteAt(i)
	}

	return nil
}

func (leaf *LeafNode) deleteAt(pos int) Node {
	leaf.data = removeLeafData(leaf.data, pos)

	var newRoot Node

	// too few keys
	if leaf.isTooFew() {
		var leftNode, rightNode *LeafNode
		if nil != leaf.getLeft() {
			leftNode, _ = leaf.getLeft().(*LeafNode)
		}
		if nil != leaf.getRight() {
			rightNode, _ = leaf.getRight().(*LeafNode)
		}

		if leftNode != nil && !checkIsTooFewKey(len(leftNode.data)-1, leaf.order) {
			// if left is available, borrow from left
			insertLeafData(&(leaf.data), 0, leftNode.data[len(leftNode.data)-1])
			leftNode.deleteAt(len(leftNode.data) - 1)

			if leaf.parent != nil {
				newRoot = leaf.parent.update()
			}
		} else if rightNode != nil && !checkIsTooFewKey(len(rightNode.data)-1, leaf.order) {
			// else if right is available, borrow from right
			leaf.data = append(leaf.data, rightNode.data[0])
			rightNode.deleteAt(0)

			if leaf.parent != nil {
				newRoot = leaf.parent.update()
			}
		} else {
			// else merge with left or right
			if leftNode != nil {
				leftNode.data = append(leftNode.data, leaf.data...)
				leaf.data = leaf.data[:0]

				newRoot = leftNode.parent.update()
			} else if rightNode != nil {
				rightNode.data = append(leaf.data, rightNode.data...)
				leaf.data = leaf.data[:0]

				newRoot = rightNode.parent.update()
			} else {
				// No node to merge
				if leaf.parent != nil {
					// is not root
					panic("No node to merge")
				}
			}

			// connecting pointers
			if leaf.prevNode != nil {
				leaf.prevNode.nextNode = leaf.nextNode
			}
			if leaf.nextNode != nil {
				leaf.nextNode.prevNode = leaf.prevNode
			}
		}
	} else if leaf.parent != nil {
		newRoot = leaf.parent.update()
	}

	return newRoot
}

func (leaf *LeafNode) getOffset(offset int) Node {
	i := 0

	if leaf.parent == nil {
		return nil
	}

	for i < len(leaf.parent.children) && leaf.parent.children[i] != leaf {
		i++
	}

	if i >= len(leaf.parent.children) || leaf.parent.children[i] != leaf {
		panic("not found in children")
	}

	if i+offset >= 0 && i+offset < len(leaf.parent.children) {
		if leaf2, ok := leaf.parent.children[i+offset].(*LeafNode); ok {
			return leaf2
		}
		panic("not leaf node")
	}
	return nil
}

func (leaf *LeafNode) getLeft() Node {
	return leaf.getOffset(-1)
}

func (leaf *LeafNode) getRight() Node {
	return leaf.getOffset(1)
}

func (leaf *LeafNode) getFirst() *LeafNode {
	return leaf
}

func (leaf *LeafNode) GetKeys() []LeafData {
	return leaf.data
}

func (leaf *LeafNode) GetNext() *LeafNode {
	return leaf.nextNode
}

func (leaf *LeafNode) inOrderPrint() {
	for i, v := range leaf.data {
		if i != 0 {
			fmt.Print(" ")
		}
		fmt.Printf("%d", v.key)
	}
}
