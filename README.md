# HW5 B+ tree

黃薰磊 408410032 kennhuang@alum.ccu.edu.tw

* 包含圖形化介面、display功能以圖形呈現
* Merge Delete 

## 語言＆版本

Golang 1.16.4

## 如何使用

首先，執行環境必須安裝好Golang

在專案的根目錄上，執行 `go run main.go`

因為圖形化介面是以網頁的形式製作，所以會需要以下步驟
螢幕上會出現
```
Enter port number for using GUI (default is 9090): 
```
請在後面輸入想要使用的Port Number，空白則會使用9090

輸入完成後，請依照程式的輸出於瀏覽器中開起相關頁面
（請注意，如果不是在本地電腦上執行，則ip可能不同）

成功完成以上步驟後，即可看到以下畫面
![](./image/homepage.png)

可以透過左側的按鈕操控B+tree
右邊會顯示樹的目前狀態

## 操作畫面

### Initialize

![](./image/Init.png)

### Attach

![](./image/Attach.png)

### Bulkload

![](./image/Bulkload.png)

### Lookup

![](./image/Lookup.png)

#### 有找到時
![](./image/Lookup_found.png)

#### 找不到時
![](./image/Lookup_not_found.png)

### Insert

![](./image/Insert.png)

### Delete

![](./image/Delete.png)

### Quit

![](./image/Quit.png)

### Display

右側顯示效果

![](./image/Display.png)

## 非自行完成部分

### 第三方Library
* [Bootstrap](https://getbootstrap.com): 網頁介面使用的Library
* [d3-graphviz](https://github.com/magjac/d3-graphviz): 將B+tree圖像化的工具

### 參考資料
* https://www.programiz.com/dsa/b-plus-tree
    * 參考B+ tree的insert流程
* https://graphviz.org/documentation/
    * 參考graphviz的語法
* https://youtu.be/pGOdeCpuwpI
    * 參考Delete過程

## BTree 部分架構

分成幾個type
* `BTree`：整顆B+Tree
* `InternalNode`：非葉節點的點
* `LeafNode`：葉節點

## 圖形介面

一開始會先開啟一個http server，包含一些操作樹的api、以及 `index.html`

輸入網址後，會被導向到 `/index.html`
這是一個靜態的html檔案，透過 `XMLHttpRequest` 與 server溝通

右邊的圖案使用 `d3-graphviz` 繪製 B+tree圖案

左邊按鈕會觸發一些彈出視窗或是功能，每個功能都是依靠server的api去操作B+tree

### API list

* `/graphviz`：提供目前B+tree的graphviz格式
* `/init`：Initailze 功能使用
* `/attach`：Attach 功能使用
* `/bulkload`：Bulkload 功能使用
* `/lookup`：Lookup 功能使用
* `/insert`：Insert 功能使用
* `/delete`：Delete 功能使用
* `/quit`：結束程式

## Merge Delete
這個 Delete 是我看過上面那個影片說明 Delete 過程後，自己寫出來的

首先，程式會先透過search找出要被刪除的資料放在哪個LeafNode，找到後透過 `(*LeafNode).deleteAt()` 刪除
刪除過程中，會先將該筆資料從葉節點中刪掉，接著檢查節點的key是否太少

如果key數量不足，檢查左邊、右邊的節點
如果左邊的節點在拿走一個key之後不會缺少，就從左邊拿一個過來
否則檢查邊節點，如果右邊的節點在拿走一個key之後不會缺少，就從右邊拿一個過來

如果上述情況都不符合，就代表左右節點的key數量剛好都是最小值，此時可以將目前節點向左或向右合併

接著，會呼叫parent節點的 `update()` 函式
這個函式會檢查所有child，以及所有child最左邊的leaf，如果有child或是child最左邊leaf是空的，就代表必須要將這個child移除
移除之後，會更新節點中所有的key，將key的值設定成右邊child的最左邊leaf的第一筆資料

接著，會檢查key是否足夠
如果太少key，就會檢查能不能從左邊、右邊拿，或是跟左右邊合併（流程大約與LeafNode相同，不再重複描述）

與LeafNode不同的地方在於，向左、右拿key時，會將左邊的key放到parent，原本在parent的key放到節點上，多餘的child移動到向別人拿key的節點上面
如果是與左、右合併時，會將parent的key拿到兩個待合併的節點之間，將原本兩個節點的child隔開

完成以上動作後，再呼叫parent的 `update()` 函式，直到樹的根為止